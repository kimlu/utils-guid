<?php
namespace kimlu\utils;

/**
 * La clase Implementa el concepto de GUID ( Globally Unique Identifier ).   
 * 
 * @author Máximo
 *
 */
abstract class GUID 
{

	/**
	 * Regex que valida un token de 32 caracteres, que combina caracteres alfabeticos (en minusculas) 
	 * y números decimales de un dígito.
	 *
	 * @var string
	 */
	const pattern = '^[a-z0-9]{32}$';
	
	/**
	 * El método valida un token de 32 caracteres alfanumerico.
	 *
	 * @param string $guid
	 * @return bool
	 */
	static public function validate( string $guid ) : bool 
	{ 
		return preg_match( '/'.GUID::pattern.'/', $guid ); 
	}
	
	/**
	 * El método genera un token de 32 caracteres alfanumerico irrepetible.
	 *
	 * @return string
	 */
	static public function generate() : string 
	{
	    $unique = microtime( TRUE ) * pi();
	    $entropy = '';
        if ( version_compare( phpversion() , '5.3.0', '>=' ) )
	    {
	        $entropy = bin2hex( openssl_random_pseudo_bytes( 16 ) );
	    }
	    else
	    {
	        $entropy = random_bytes( 16 ).memory_get_usage();
	    }
	    return strtolower( md5( $unique.$entropy ) );
	}
	
}