<?php
namespace tests\kimlu\utils\guid;

use PHPUnit\Framework\TestCase;
use kimlu\utils\GUID;

/**
 *
 * @author Máximo
 *        
 */
final class GUIDTest extends TestCase
{
    
    /**
     * 
     * @var string
     */
    private $guidValue = ''; 
    
    /**
     * 
     * {@inheritDoc}
     * @see \PHPUnit\Framework\TestCase::setUp()
     */
    public function setUp(): void
    {
        $this->guidValue = GUID::generate();
    }    
    
    public function testGenerateTokenIsString()
    {
        $this->assertIsString( $this->guidValue );
    }
    
    public function testGenerateTokenStringSize()
    {
        $this->assertEquals( 32, strlen( $this->guidValue ) );
    }
    
    public function testValidateToken()
    {
        $this->assertTrue( GUID::validate( $this->guidValue ) );
    }
    
}

